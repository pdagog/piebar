# This Makefile is only used for generating the web site

all:	piebar.pdf index.html gallery

piebar.man: piebar.md
	pandoc -s -t man $< > $@
piebar.pdf: piebar.man
	groff -man -Tps $< | ps2pdf - - > $@

index.html: README.md
	pandoc \
            --standalone \
            --metadata="pagetitle:Piebar -- Simple Pie and Bar graph creation" \
            --variable='lang:en' \
            --variable='author-meta:Pierre David' \
            --variable='highlighting-css: *{font-family:Arial,sans-serif;}' \
            -o index.html \
            README.md

gallery:
	python3 piebar.py -o gal-pie1.svg -t "Sales by product" pie -s 25 < sales1.dat
	python3 piebar.py -o gal-pie1custom.svg -t "Sales by product with custom colors" pie -s -25 "Product A|cadetblue" "Product B|khaki" "Product C|seagreen" "Product D|lightcoral" < sales1.dat
	python3 piebar.py -o gal-bar1.svg -t "Sales by product" bar < sales1.dat
	python3 piebar.py -o gal-bar1custom.svg -t "Sales by product with custom colors" bar -g "Product A|cadetblue" "Product B|khaki" "Product C|seagreen" "Product D|lightcoral" < sales1.dat
	python3 piebar.py -o gal-bar1stack.svg -t "Sales by product, stacked" bar -s < sales1.dat
	python3 piebar.py -o gal-bar1stackcustom.svg -t "Sales by product with custom colors" bar -s -g "Product A|cadetblue" "Product B|khaki" "Product C|seagreen" "Product D|lightcoral" < sales1.dat
	python3 piebar.py -o gal-bar2.svg -t "Sales by product and quarter" bar < sales2.dat
	python3 piebar.py -o gal-bar2stack.svg -t "Sales by product, stacked by quarter (with custom colors)" bar -s "Q1|lightblue" "Q2|gold" < sales2.dat
	python3 piebar.py -o gal-bar3.svg -t "Sales by product, quarter (left: Q1, right: Q2) and region" bar -g < sales3.dat

clean:
	rm -f piebar.man *.pdf *.svg index.html
