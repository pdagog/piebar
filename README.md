Simple Pie and Bar chart creation
=================================

Features
--------

The **piebar** program aims to simplify the creation of pie and bar
charts from command line or shell scripts.
You just give your series of data on standard input, and voilà !

  * very easy to use
  * 6 types of charts (pie charts, grouped bars with one or two keys, stacked bar from one to three keys)
  * customisable title, legends or colors
  * reasonable defaults
  * output various formats (PDF, PNG, SVG, etc.)
  * relies on [matplotlib](https://matplotlib.org/) only, no other dependencies
  * program is contained in one single Python file
  * program is easy to install or to run without installing it

Manual
------

PDF manual page is [available here](https://pdagog.gitlab.io/piebar/piebar.pdf).

Gallery
-------

### Pie charts

File used in these examples: <https://gitlab.com/pdagog/piebar/-/blob/main/sales1.dat>

#### Sales by product

![Sales by product](https://pdagog.gitlab.io/piebar/gal-pie1.svg)

	piebar -o gal-pie1.svg -t "Sales by product" pie -s -25 < sales1.dat

#### Sales by product with custom colors

![Sales by product with custom colors](https://pdagog.gitlab.io/piebar/gal-pie1custom.svg)

	piebar -o gal-pie1custom.svg -t "Sales by product with custom colors" pie -s -25 "Product A|cadetblue" "Product B|khaki" "Product C|seagreen" "Product D|lightcoral" < sales1.dat

### Bar charts with one key

File used in these examples: <https://gitlab.com/pdagog/piebar/-/blob/main/sales1.dat>

#### Sales by product

![Sales by product](https://pdagog.gitlab.io/piebar/gal-bar1.svg)

	piebar -o gal-bar1.svg -t "Sales by product" bar < sales1.dat

#### Sales by product wih custom colors and a grid

![Sales by product wih custom colors](https://pdagog.gitlab.io/piebar/gal-bar1custom.svg)

	piebar -o gal-bar1custom.svg -t "Sales by product with custom colors" bar -g "Product A|cadetblue" "Product B|khaki" "Product C|seagreen" "Product D|lightcoral" < sales1.dat

#### Sales by product wih stacked bars

![Sales by product wih stacked bars](https://pdagog.gitlab.io/piebar/gal-bar1stack.svg)

	piebar -o gal-bar1stack.svg -t "Sales by product, stacked" bar -s < sales1.dat

### Sales by product wih stacked bars, custom colors and a grid

![Sales by product wih stacked bars and custom colors](https://pdagog.gitlab.io/piebar/gal-bar1stackcustom.svg)

	piebar -o gal-bar1stackcustom.svg -t "Sales by product with custom colors" bar -s "Product A|cadetblue" "Product B|khaki" "Product C|seagreen" "Product D|lightcoral" < sales1.dat

### Bar charts with two keys

File used in these examples: <https://gitlab.com/pdagog/piebar/-/blob/main/sales2.dat>

#### Sales by product and quarter

![Sales by product and quarter](https://pdagog.gitlab.io/piebar/gal-bar2.svg)

	piebar -o gal-bar2.svg -t "Sales by product and quarter" bar < sales2.dat

#### Sales by product, stacked by quarter

![Sales by product, stacked by quarter](https://pdagog.gitlab.io/piebar/gal-bar2stack.svg)

	piebar -o gal-bar2stack.svg -t "Sales by product, stacked by quarter (with custom colors)" bar -s "Q1|lightblue" "Q2|gold" < sales2.dat

### Bar charts with three keys

File used in this example: <https://gitlab.com/pdagog/piebar/-/blob/main/sales3.dat>

#### Sales by product, quarter and region

![Sales by product, quarter and region](https://pdagog.gitlab.io/piebar/gal-bar3.svg)

	piebar -o gal-bar3.svg -t "Sales by product, quarter (left: Q1, right: Q2) and region" bar < sales3.dat

Releases
--------

  * Release 1.0dev:
    * Add the -g option to display a grid on Y axis
    * Remove redundant green in default colors
  * Release 0.9:
    * Initial version
