% PIEBAR(1) man page
% Pierre David
% June 3, 2024

# NAME

piebar - generate simple pie or bar charts


# SYNOPSIS

piebar [*GENERAL OPTIONS*] pie [*PIE OPTIONS*] [*LEGENDS*]

piebar [*GENERAL OPTIONS*] bar [*BAR OPTIONS*] [*LEGENDS*]


# DESCRIPTION

Generate simple pie or bar charts from one or more data series presented
on standard input.

## Data series

Data are made of one, two or three keys and a value, separated by a
delimiter ("**|**" by default). They are always given on standard input.

Example 1: product sales (one key, one value)

    Product A|50
    Product B|30
    Product C|20
    Product D|10

Example 2: product sales by product, quarter and region (three keys and
one value)

    Product A|Q1|North|50
    Product A|Q2|North|55
    Product B|Q1|North|30
    Product B|Q2|North|35
    Product A|Q1|South|40
    Product A|Q2|South|45
    Product B|Q1|South|20
    Product B|Q2|South|25

## Graph generation

Graphs are generated as PDF or any other format supported by the
matplotlib library (SVG, PNG, PGF, EPS, etc.). By default, they are
written on standard output, unless a file name is given with the **-o**
option. If no file extension is provided with this option, format is
specified with the **-f** option.

Pie charts require a series of data with a single key on standard input.

Bar charts require data series with 1, 2 or 3 keys on the standard
input.  When given 2 keys, second level keys are grouped around each
first level key.  Last key bars may also be stacked with the **-s**
option. If 3 keys are given, the third is always stacked.

## Legends

Legends on the graph are automatically deduced from the keys in the
data series.

However, one can use the *LEGENDS* arguments to provide order and
optionnaly colors using the same delimiter as for data. For example,
the command:

    piebar -o out.pdf bar "Product A|blue" "Product B" < sales1.dat

generates a bar graph with a blue bar for "Product A" first, then a bar
with the default color for "Product B". No other bar is generated since
at least one key is found in the legend.

All matplotlib colors may be used (see
https://matplotlib.org/stable/users/explain/colors/colors.html#colors-def)


## General options

General options must be provided before the `pie` or `bar` keyword.

-h, \--help
  : display help message and exit

-v, \--verbose
  : display verbose informations

-V, \--version
  : display version and exit

-d, \--delimiter *DELIM*
  : use *DELIM* instead of "**|**" for column delimiter

-o, \--output *FILE*
  : generate chart into the given file name and format (according to
    file extension).

-f, \--format *FORMAT*
  : use the specified format (`pdf`, `svg`, `png`, etc.) for chart
    generation.
    Use this option only when chart is generated on standard output
    since the format is deduced from the file name extension provided
    with the `-o` option.

-t, \--title *TITLE*
  : add title on the generated figure.

# PIE CHARTS

With the `pie` argument, `piebar` generates a pie chart. Only one
data serie must be provided on standard input.

## Pie options

-h, \--help
  : display help message and exit

-s, \--start *ANGLE*
  : start the first pie slice at the specified angle (0..360 degrees)

# BAR CHARTS

With the `bar` argument, `piebar` generates a bar chart. Data series
must include 1 to 3 keys.

## Bar options

-h, \--help
  : display help message and exit

-g, \--grid
  : display a grid on y axis

-s, \--stacked
  : bars for the last key must be stacked.
    When given 3 keys, the last key is always stacked, whether *-s* is
    specified or not.

-r, \--rotlbl *ANGLE*
  : rotate labels on the X axis with the specified angle (0..360 degrees),
    often used when labels are too wide.

# EXIT STATUS

This program exits 0 on success, and 1 if an error occurs.


# EXAMPLES

Pie chart for product sales, with a figure title and an initial angle
of 25 degree:

    > piebar -o sales.pdf -t "Product sales" pie -s 25 < sales1.dat

SVG bar chart for product sales by quarter and region. Bar are grouped
by quarters (second level key) around each product (first level key),
and sales are stacked by region (third key). Legends with colors are
provided and labels on x axis are rotated with an angle of 15 degrees:

    > piebar -o sales.svg bar -r 15 "South|gold" "North|blue" < sales3.dat

# SEE ALSO

matplotlib <https://matplotlib.org/>
