#!/usr/bin/env python3

#
# Simple Pie and Bar graphs creation
#
# Author : Pierre David
#

import os
import io
import sys
import argparse

import matplotlib.pyplot as plt

version = '1.0-dev'
verbose = False

defcolors = ['green', 'orange', 'steelblue', 'cyan', 'blueviolet', 'coral', 'palegreen', 'cornflowerblue', 'red', 'goldenrod', 'firebrick']

##############################################################################
# Utilities
##############################################################################

def error (msg, line=None):
    if line is not None:
        msg = 'Line {}: '.format (line) + msg
    print (msg, file=sys.stderr)
    sys.exit (1)

def verbositer (msg):
    if verbose:
        print (msg, file=sys.stderr)

def write_graph (output, format):
    plt.draw ()
    if output is None:
        buf = io.BytesIO ()
        if format is None:
            format = 'pdf'
        plt.savefig (buf, format=format)
        sys.stdout.buffer.write (buf.getvalue ())
    else:
        plt.savefig (output, format=format)
    plt.close ()
    return

def parse_legends (legends, delim):
    #
    # Legend processing: each legend is a text, or text|color
    #

    newl = []
    colors = {}
    for l in legends:
        tab = l.strip ().split (delim)
        match len(tab):
            case 1:
                newl.append (tab [0])
            case 2:
                newl.append (tab [0])
                colors [tab [0]] = tab [1]
            case _:
                error ('unrecognized legend "{}"'.format (l))
    legends = newl

    verbositer ('parse_legends -> legends = {}'.format (legends))
    verbositer ('parse_legends -> colors  = {}'.format (colors))

    return (legends, colors)

def read_values (mincol, maxcol, delim, legends):
    nkeys = None
    keys = {}
    values = {}
    lineno = 0
    for line in sys.stdin:
        lineno += 1

        colvals = line.strip ().split (delim)
        ncol = len (colvals)

        # Ignore empty lines or comments
        if ncol == 0 or line [0] == '#':
            continue

        if nkeys is None:
            nkeys = ncol - 1
            if nkeys < mincol or nkeys > maxcol:
                error ('should be {} to {} keys ({})'.
                                format (lineno, mincol, maxcol, nkeys), lineno)
            for i in range (0, ncol):
                keys [i] = []

        if nkeys != ncol - 1:
            error ('{}: inconsistent column count ({})'.format (lineno, ncol), lineno)

        val = colvals [ncol-1]
        try:
            val = float (val)
        except Exception as e:
            error ('{}: invalid value ({})'.format (lineno, val), lineno)
        del colvals[ncol-1]

        #
        # Accumulate keys and values:
        #   values [k0, k1 ...] = v
        #   keys [0] = [k, k ...]
        #   keys [1] = [k, k ...]
        #
        idx = delim.join (colvals)
        if idx not in values:
            values [idx] = 0
        values [idx] += val

        for i, k in enumerate (colvals):
            if k not in keys [i]:
                keys [i].append (k)

    if len(values) == 0:
        error ('No value found')

    verbositer ('read_values -> values={}'.format(values))

    # Build sorted lists of keys for each level
    lkeys = {}
    for i in range(nkeys):
        #
        # If no key appears as a legend on the command line,
        # keep the order of values presented on stdin.
        # build a sorted list with the keys encountered on stdin.
        # Else, use the order of legends on the command line to
        # build the sorted list.
        #
        sort = True
        # Do any of the encountered keys is found in the legend arguments?
        for k in keys [i]:
            if k in legends:
                sort = False
                break
        if sort:
            # Simply sort the keys
            lkeys [i] = sorted (keys [i])
        else:
            # Build a list with legends given as arguments
            lkeys [i] = []
            for k in legends:
                if k in keys [i]:
                    lkeys [i].append (k)

    for i in range(nkeys):
        verbositer ('read_values -> lkeys [{}] = {}'.format (i, lkeys [i]))

    return (lkeys, values)

##############################################################################
# Pie chart
##############################################################################

def do_pie (args):
    delim = args.delimiter
    output = args.output
    format = args.format
    title = args.title
    legends = args.legends
    start = args.start

    (legends, colors) = parse_legends (legends, delim)

    (lkeys, values) = read_values (1, 1, delim, legends)

    if title is not None:
        plt.title (title)

    sizes = []
    col = []
    for i0, k0 in enumerate (lkeys [0]):
        sizes.append (values [k0])
        c = colors [k0] if k0 in colors else defcolors [i0]
        col.append (c)

    verbositer ('do_pie -> colors={}'.format (colors))

    plt.pie (sizes, labels=lkeys [0],
                labeldistance=1.03, autopct='%1.1f %%',
                colors=col, startangle=start)
    plt.axis ('equal')
    write_graph (output, format)

##############################################################################
# Bar chart
##############################################################################

def do_bar (args):
    delim = args.delimiter
    output = args.output
    format = args.format
    title = args.title
    legends = args.legends
    stacked = args.stacked
    grid = args.grid

    (legends, colors) = parse_legends (legends, delim)

    #
    # Available bar charts, according to columns on stdin:
    #   key | val
    #       traditionnal bar chart
    #   key | val                   (with --stacked)
    #       only one bar with all keys stacked
    #   key1 | key2 | val
    #       key2 bars are grouped on x axis around each key1
    #   key1 | key2 | val           (with --stacked)
    #       bars are key2-stacked on x axis on each key1
    #   key1 | key2 | key3 | val    (--stacked implied)
    #       bars are key3-stacked on key2 bars grouped around each key1
    #

    nkeys = None

    (lkeys, values) = read_values (1, 3, delim, legends)

    # print ('values={}'.format(values))

    if title is not None:
        plt.title (title)

    match len(lkeys):
        case 1:
            if stacked:
                bar1stack (args, lkeys, values, colors)
            else:
                bar1 (args, lkeys, values, colors)
        case 2:
            if stacked:
                bar2stack (args, lkeys, values, colors)
            else:
                bar2 (args, lkeys, values, colors)
        case 3:
            bar3stack (args, lkeys, values, colors)

    if grid:
        plt.grid (axis='y', alpha=0.4)

    plt.draw ()
    write_graph (output, format)
    return


def bar1 (args, lkeys, values, colors):
    rotlbl = args.rotlbl
    x = 1
    for i0, k0 in enumerate (lkeys [0]):
        c = colors [k0] if k0 in colors else defcolors [i0]
        plt.bar (x, values [k0], color=c)
        x += 1
    plt.xticks (range(1, len(lkeys [0])+1), lkeys [0], rotation=rotlbl)
    return

def bar1stack (args, lkeys, values, colors):
    rotlbl = args.rotlbl
    height = []
    xtab = []
    bottom = []
    col = []
    y = 0
    for i0, k0 in enumerate (lkeys [0]):
        xtab.append (1)
        c = colors [k0] if k0 in colors else defcolors [i0]
        col.append (c)
        idx = k0
        val = values [idx] if idx in values else 0
        bottom.append (y)
        height.append (val)
        y += val
    color = defcolors [0:len(xtab)]
    plt.bar (xtab, height, bottom=bottom, label=lkeys[0], color=col)
    plt.legend ()
    plt.xticks ([])
    return

def bar2 (args, lkeys, values, colors):
    rotlbl = args.rotlbl
    delim = args.delimiter

    grpsize = len (lkeys [1])
    width = 1 - 0.4
    barwidth = width / (grpsize + (grpsize - 1)/2)
    interbar = barwidth / 2

    for i1, k1 in enumerate (lkeys [1]):
        x = 1 - width/2 + barwidth / 2
        if i1 > 0:
            x += (i1 * barwidth + i1 * interbar)
        height = []
        xtab = []
        c = colors [k1] if k1 in colors else defcolors [i1]

        for k0 in lkeys [0]:
            xtab.append (x)
            idx = delim.join ([k0, k1])
            val = values [idx] if idx in values else 0
            height.append (val)
            x += 1
        plt.bar (xtab, height, width=barwidth, label=k1, color=c)

    plt.xticks (range(1, len(lkeys [0])+1), lkeys [0], rotation=rotlbl)
    plt.legend ()
    return

def bar2stack (args, lkeys, values, colors):
    rotlbl = args.rotlbl
    delim = args.delimiter

    bottom = [0 for k0 in lkeys [0]]

    for i1, k1 in enumerate (lkeys [1]):
        x = 1
        xtab = []
        height = []
        newbottom = []
        c = colors [k1] if k1 in colors else defcolors [i1]

        for i0, k0 in enumerate (lkeys [0]):
            xtab.append (x)
            idx = delim.join ([k0, k1])
            val = values [idx] if idx in values else 0
            height.append (val)
            newbottom.append (bottom [i0] + val)
            x += 1

        plt.bar (xtab, height, bottom=bottom, label=k1, color=c)
        bottom = newbottom

    plt.xticks (range(1, len(lkeys [0])+1), lkeys [0], rotation=rotlbl)
    plt.legend ()
    return

def bar3stack (args, lkeys, values, colors):
    rotlbl = args.rotlbl
    delim = args.delimiter

    grpsize = len (lkeys [1])
    width = 1 - 0.4
    barwidth = width / (grpsize + (grpsize - 1)/2)
    interbar = barwidth / 2

    bottom = []
    for i0 in lkeys [0]:
        for i1 in lkeys [1]:
            bottom.append (0)

    for i2, k2 in enumerate (lkeys [2]):
        xtab = []
        height = []
        newbottom = []
        c = colors [k2] if k2 in colors else defcolors [i2]
        j = 0

        for i0, k0 in enumerate (lkeys [0]):
            for i1, k1 in enumerate (lkeys [1]):

                x = (i0 + 1) - width/2 + barwidth / 2
                if i1 > 0:
                    x += (i1 * barwidth + i1 * interbar)
                xtab.append (x)

                idx = delim.join ([k0, k1, k2])
                val = values [idx] if idx in values else 0

                height.append (val)
                newbottom.append (bottom [j] + val)
                j += 1

        plt.bar (xtab, height, bottom=bottom, width=barwidth, label=k2, color=c)
        bottom = newbottom

    plt.xticks (range(1, len(lkeys [0])+1), lkeys [0], rotation=rotlbl)
    plt.legend ()
    return

##############################################################################
# MAIN
##############################################################################

def main ():
    p = argparse.ArgumentParser (description='Graph generator')
    p.add_argument ('-v', '--verbose', action='store_true',
                                help='verbose messages')
    p.add_argument ('-V', '--version', action='version', version=version,
                                help='display version and exit')
    p.add_argument ('-d', '--delimiter', action='store', default='|',
                                help='column delimiter')
    p.add_argument ('-o', '--output', action='store', metavar='FILE',
                                help='output file')
    p.add_argument ('-f', '--format', action='store',
                                help='format (pdf, svg, png...)')
    p.add_argument ('-t', '--title', action='store', help='graph title')
    subp = p.add_subparsers (help='graph action')

    # pie output.pdf title [angle]
    p_pie = subp.add_parser ('pie', help='pie graph')
    p_pie.add_argument ('-s', '--start', type=float, default=0,
                        help='start angle [0..360]')
    p_pie.add_argument ('legends', help='sorted legends', nargs='*')
    p_pie.set_defaults (func=do_pie)

    # bar output.pdf title
    p_bar = subp.add_parser ('bar', help='bar graph')
    p_bar.add_argument ('legends', help='sorted legends', nargs='*')
    p_bar.add_argument ('-s', '--stacked', action='store_true',
                        help='last column is stacked')
    p_bar.add_argument ('-r', '--rotlbl', type=float, default=0,
                        help='label rotation [0..360]')
    p_bar.add_argument ('-g', '--grid', action='store_true',
                        help='label rotation [0..360]')
    p_bar.set_defaults (func=do_bar)

    args = p.parse_args ()

    global verbose
    verbose = args.verbose

    if hasattr (args, 'func'):
        try:
            args.func (args)
        except KeyboardInterrupt:
            sys.exit (1)
    else:
        p.print_help ()
        sys.exit (1)

    sys.exit (0)

if __name__ == '__main__':
    main ()
